class MarkdonwGeneration:
    @classmethod
    def build(cls, repos):
        items= ''.join(
            f'**ID:** {repo.id} **NAME:** {repo.name} **STARS:** {repo.starts}\n' 
               for repo in repos)

        return f'##REPOS\n\n{items}'       