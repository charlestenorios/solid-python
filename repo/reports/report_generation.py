# from .html_generation import HtmlGeneration usanva no modelo errado de fazer
# from .makdonw_generation import MarkdonwGeneration

class ReportGenretion:
     # maneira correta de implementar com injeção de dependência
     # no lugar de eu passar o tipo vou passar o gerador
    @classmethod
    def build(cls, generador, repos):
        return generador.build(repos)   
    '''
    maneira errda de implementar
    @classmethod
    def build(cls, type, repos):
        ## problema dessa codigo e q vc tem muitos ifs e se eu precisar regar mais um relatorio em outro formato vou precisar mexer aqui
        ## ess classe nao está aberta e extensão ela fere o segundo principio do solid
        ### a solucçaõ e injetar um dependência o gerador q a gente precisar
        if type=='HTML':
            return HtmlGeneration.build(repos)
        elif type=='MARKDOWN':
            return MarkdonwGeneration.build(repos)
        else:
            return "report invalid type"    
    ''' 

