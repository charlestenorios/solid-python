from github.client import GithubClient
from repo.parse import RepoParser
from repo.reports.report_generation import ReportGenretion
from repo.reports.makdonw_generation import MarkdonwGeneration
from repo.reports.html_generation import HtmlGeneration


if __name__ == '__main__':
    username = 'charlestenorio'
    response = GithubClient.get_repositorie_user(username)
    if response["status_code"]==200:
        repos=RepoParser.parse(response['body'])
        makdon_report = ReportGenretion.build(MarkdonwGeneration, repos)
        html_report = ReportGenretion.build(HtmlGeneration, repos)
        print(html_report)
        print(makdon_report)
    else:
        print(response['body'])    

